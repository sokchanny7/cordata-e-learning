//
//  ViewController.swift
//  coredata-e-learning
//
//  Created by iMac on 28/12/2016.
//  Copyright © 2016 nini. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    func insert(){
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let student     = Student(context: managedContext)
        student.id      = 1
        student.name    = "Sok Chann"
        appDelegate.saveContext()
        
    }
    
    func save(){
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "Student", in: managedContext)!
        let student = NSManagedObject(entity: entity, insertInto: managedContext)
        
        student.setValue("Sok Chann", forKeyPath: "name")
        student.setValue(1, forKeyPath: "id")
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }

}

